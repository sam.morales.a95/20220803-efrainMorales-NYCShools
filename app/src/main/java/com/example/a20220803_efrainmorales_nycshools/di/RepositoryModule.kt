package com.example.a20220803_efrainmorales_nycshools.di

import com.example.a20220803_efrainmorales_nycshools.data.SchoolRepositoryImpl
import com.example.a20220803_efrainmorales_nycshools.domain.SchoolRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    @Singleton
    abstract fun bindSchoolRepository(
        repository: SchoolRepositoryImpl
    ): SchoolRepository
}