package com.example.a20220803_efrainmorales_nycshools.domain

interface SchoolRepository {
    suspend fun getSchools(): List<School>?
    suspend fun getSchoolScores(schoolId: String): Scores?
}