package com.example.a20220803_efrainmorales_nycshools.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class School(
    val id: String = "",
    val name: String = "",
    val location: String = "",
    val description: String = "",
    val website: String = "",
    val email: String = ""
): Parcelable