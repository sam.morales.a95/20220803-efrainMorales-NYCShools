package com.example.a20220803_efrainmorales_nycshools.domain

data class Scores(
    val mathAvg: String,
    val readingAvg: String,
    val writingAvg: String
)