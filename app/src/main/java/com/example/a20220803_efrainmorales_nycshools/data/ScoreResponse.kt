package com.example.a20220803_efrainmorales_nycshools.data


import com.google.gson.annotations.SerializedName

data class ScoreResponse(
    @SerializedName("dbn")
    val dbn: String?,
    @SerializedName("num_of_sat_test_takers")
    val numOfSatTestTakers: String?,
    @SerializedName("sat_critical_reading_avg_score")
    val satCriticalReadingAvgScore: String?,
    @SerializedName("sat_math_avg_score")
    val satMathAvgScore: String?,
    @SerializedName("sat_writing_avg_score")
    val satWritingAvgScore: String?,
    @SerializedName("school_name")
    val schoolName: String?
)