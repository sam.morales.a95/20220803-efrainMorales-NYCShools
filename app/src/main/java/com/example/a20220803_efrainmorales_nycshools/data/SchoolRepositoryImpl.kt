package com.example.a20220803_efrainmorales_nycshools.data

import com.example.a20220803_efrainmorales_nycshools.domain.School
import com.example.a20220803_efrainmorales_nycshools.domain.SchoolRepository
import com.example.a20220803_efrainmorales_nycshools.domain.Scores
import java.lang.Exception
import javax.inject.Inject

class SchoolRepositoryImpl @Inject constructor(
    private val service: SchoolService
): SchoolRepository {
    override suspend fun getSchools(): List<School>? {
        return try {
            val response = service.fetchSchools()
            if(response.isSuccessful){
                response.body()?.map { it.toSchool() }
            }else{
                null
            }
        } catch (e: Exception){
            null
        }
    }

    override suspend fun getSchoolScores(schoolId: String): Scores? {
        val response = service.fetchSchoolScores(schoolId)
        return try {
            if(response.isSuccessful){
                return response.body()?.firstOrNull()?.toScore()
            } else {
                return null
            }
        }catch (e: Exception){
            null
        }

    }
}