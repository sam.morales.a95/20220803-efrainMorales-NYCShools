package com.example.a20220803_efrainmorales_nycshools.data

import com.example.a20220803_efrainmorales_nycshools.domain.School
import com.example.a20220803_efrainmorales_nycshools.domain.Scores

fun ScoreResponse.toScore() = Scores(
    mathAvg = satMathAvgScore ?: "",
    readingAvg = satCriticalReadingAvgScore ?: "",
    writingAvg = satWritingAvgScore ?: ""
)

fun SchoolResponse.toSchool() = School(
    id = dbn ?: "",
    name = schoolName ?: "",
    location = location ?: "",
    description = overviewParagraph ?: "",
    website = website ?: "",
    email = schoolEmail ?: ""
)