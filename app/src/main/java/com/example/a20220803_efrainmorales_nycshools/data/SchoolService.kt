package com.example.a20220803_efrainmorales_nycshools.data

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolService {
    @GET("s3k6-pzi2.json")
    suspend fun fetchSchools(): Response<List<SchoolResponse>>

    @GET("f9bf-2cp4.json")
    suspend fun fetchSchoolScores(
        @Query("dbn") schoolDbn: String
    ): Response<List<ScoreResponse>>
}