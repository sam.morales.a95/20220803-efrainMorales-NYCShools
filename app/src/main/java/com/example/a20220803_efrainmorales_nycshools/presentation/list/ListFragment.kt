package com.example.a20220803_efrainmorales_nycshools.presentation.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.a20220803_efrainmorales_nycshools.databinding.FragmentListBinding
import com.example.a20220803_efrainmorales_nycshools.domain.School
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ListFragment : Fragment() {
    private lateinit var binding: FragmentListBinding
    private val adapter = SchoolAdapter(::onSchoolClicked)
    private val viewModel by viewModels<ListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListBinding.inflate(layoutInflater, container, false)
        binding.schoolsList.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.schools.observe(viewLifecycleOwner){
            adapter.submitList(it)
        }
    }

    private fun onSchoolClicked(school: School) {
        findNavController().navigate(ListFragmentDirections.actionListFragmentToDetailsFragment(school))
    }
}