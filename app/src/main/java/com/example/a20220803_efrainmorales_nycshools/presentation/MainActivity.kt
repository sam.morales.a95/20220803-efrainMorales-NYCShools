package com.example.a20220803_efrainmorales_nycshools.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.a20220803_efrainmorales_nycshools.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}