package com.example.a20220803_efrainmorales_nycshools.presentation.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220803_efrainmorales_nycshools.domain.School
import com.example.a20220803_efrainmorales_nycshools.domain.SchoolRepository
import com.example.a20220803_efrainmorales_nycshools.domain.Scores
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(
    private val repository: SchoolRepository
): ViewModel() {
    private val _scores = MutableLiveData<Scores>()
    val scores: LiveData<Scores>
        get() = _scores

    fun getSchoolDetails(school: School){
        viewModelScope.launch(Dispatchers.IO) {
            val result = repository.getSchoolScores(school.id)
            result?.let {
                _scores.postValue(result)
            }
        }
    }
}