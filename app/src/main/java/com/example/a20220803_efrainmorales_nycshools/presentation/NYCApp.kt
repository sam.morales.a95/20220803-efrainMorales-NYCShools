package com.example.a20220803_efrainmorales_nycshools.presentation

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCApp: Application() {

}