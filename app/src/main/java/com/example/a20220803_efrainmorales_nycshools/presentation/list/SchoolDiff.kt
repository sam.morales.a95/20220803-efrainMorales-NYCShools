package com.example.a20220803_efrainmorales_nycshools.presentation.list

import androidx.recyclerview.widget.DiffUtil
import com.example.a20220803_efrainmorales_nycshools.domain.School

class SchoolDiff: DiffUtil.ItemCallback<School>() {
    override fun areItemsTheSame(oldItem: School, newItem: School): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: School, newItem: School): Boolean {
        return oldItem == newItem
    }
}