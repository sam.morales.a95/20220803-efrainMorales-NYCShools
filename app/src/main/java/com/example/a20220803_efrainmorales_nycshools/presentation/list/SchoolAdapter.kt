package com.example.a20220803_efrainmorales_nycshools.presentation.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220803_efrainmorales_nycshools.databinding.SchoolItemViewBinding
import com.example.a20220803_efrainmorales_nycshools.domain.School

class SchoolAdapter(
    private val listener: (School) -> Unit
) : ListAdapter<School, SchoolAdapter.SchoolViewHolder>(SchoolDiff()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        return SchoolViewHolder(
            SchoolItemViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class SchoolViewHolder(private val itemBinding: SchoolItemViewBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(school: School) {
            with(itemBinding) {
                schoolName.text = school.name
                schoolLocation.text = school.location
                schoolShowDetails.setOnClickListener { listener(school) }
            }
        }
    }
}