package com.example.a20220803_efrainmorales_nycshools.presentation.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.a20220803_efrainmorales_nycshools.R
import com.example.a20220803_efrainmorales_nycshools.databinding.FragmentDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsFragment : Fragment() {
    private val viewModel by viewModels<DetailsViewModel>()
    private lateinit var binding: FragmentDetailsBinding
    private val args by navArgs<DetailsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding){
            schoolDetailsName.text = args.schoolSelected.name
            schoolDetailsDescription.text = args.schoolSelected.description
            schoolDetailsEmail.text = args.schoolSelected.email
            schoolDetailsWebsite.text = args.schoolSelected.website
            schoolDetailsMath.text = getString(R.string.math_label_format, "-")
            schoolDetailsReading.text = getString(R.string.reading_label_format, "-")
            schoolDetailsWriting.text = getString(R.string.writing_label_format, "-")
        }
        setupObservers()
        viewModel.getSchoolDetails(args.schoolSelected)
    }

    private fun setupObservers() {
        viewModel.scores.observe(viewLifecycleOwner){
            with(binding){
                schoolDetailsMath.text = getString(R.string.math_label_format, it.mathAvg)
                schoolDetailsReading.text = getString(R.string.reading_label_format, it.readingAvg)
                schoolDetailsWriting.text = getString(R.string.writing_label_format, it.writingAvg)
            }
        }
    }
}