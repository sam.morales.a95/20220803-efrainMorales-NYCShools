package com.example.a20220803_efrainmorales_nycshools.presentation.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220803_efrainmorales_nycshools.domain.School
import com.example.a20220803_efrainmorales_nycshools.domain.SchoolRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListViewModel @Inject constructor(
    private val repository: SchoolRepository
): ViewModel() {

    private val _schools = MutableLiveData<List<School>>()
    val schools: LiveData<List<School>>
        get() = _schools

    init {
        refreshSchools()
    }

    fun refreshSchools(){
        /*_schools.postValue(
            listOf(
                School(
                    "21K728",
                    "Test School",
                    "Location attribute",
                    "The mission of Liberation Diploma Plus High School, in partnership with CAMBA, is to develop the student academically, socially, and emotionally. We will equip students with the skills needed to evaluate their options so that they can make informed and appropriate choices and create personal goals for success. Our year-round model (trimesters plus summer school) provides students the opportunity to gain credits and attain required graduation competencies at an accelerated rate. Our partners offer all students career preparation and college exposure. Students have the opportunity to earn college credit(s). In addition to fulfilling New York City graduation requirements, students are required to complete a portfolio to receive a high school diploma.",
                    "www.google.com",
                    "test@test.com"
                )
            )
        )*/
        viewModelScope.launch(Dispatchers.IO){
            val result = repository.getSchools()
            _schools.postValue(result)
        }
    }

}